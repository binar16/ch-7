const passport = require("passport");
const { Users } = require ('../models')

const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");

passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromHeader("authorization"),
      secretOrKey: "secret",
    },
    
    async (payload, done) => {
      const user = await Users.findByPk(payload.id)
      done(null, user);
    }
  )
);

module.exports = passport;
