const express = require("express");
const log = require("./modules/log");
const auth = require("./modules/auth");
const regis = require("./modules/regis");
const room = require("./modules/room");
const gameplay = require("./modules/gameplay");
const bodyParser = require("body-parser");


const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.use(log)

app.use(regis);

app.use(auth);

app.use(room);

app.use(gameplay);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
