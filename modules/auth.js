const express = require("express");
const app = express.Router();
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken")

const { Users } = require ('../models')

app.post('/auth', async (req, res) => {
    const payload = req.body
    const user = await Users.findOne({
        where: { 
            username: payload.username
        }
    })

    if(!user){
        res.status(401).send("salah")
        return
    }

    const correctPassword = await bcrypt.compare(payload.password, user.password)
    if(!correctPassword){
        res.status(401).send("salah")
        return
    }

    const token = jwt.sign({
        id: user.id
    }, "secret")
    console.log(token)

    res.send("ok")
})

module.exports = app