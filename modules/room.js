const express = require("express");
const passport = require("../lib/passport");

const { Rooms, RoomPlayers } = require("../models");

const app = express.Router();

const roomCode = (length) => {
  var result = "";
  var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

app.post('/room',passport.authenticate("jwt", { session: false }), async (req, res) => {
    const payload = {
        code: roomCode(4),
        roundNum: req.body.roundNum,
        status: "ACTIVE",
        createdBy: req.user.id
    }

    const room = await Rooms.create(payload)

    await RoomPlayers.create({
      roomId: room.id,
      playerId: req.user.id
    })

    res.send({
        code: room.code,
        roundNum: room.roundNum
    })
})

module.exports = app