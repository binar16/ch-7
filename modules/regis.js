const express = require("express");
const app = express.Router();
const bcrypt = require('bcrypt');

const { Users } = require('../models')

app.post('/regis', async (req, res) => {
    const payload = req.body
    const encryptPassword = await bcrypt.hash(payload.password, 10)
    
    payload.role = "PLAYER"
    payload.password = encryptPassword
    
    const user = await Users.create(payload)
    
    res.send({
        id: user.id,
        email: user.email,
        username: user.username,
        name: user.name
    })
})

module.exports = app