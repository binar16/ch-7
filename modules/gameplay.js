const express = require("express");
const app = express.Router();

const passport = require("../lib/passport");

const {
  Rooms,
  RoomPlayers,
  GameRounds,
  GameRoundHistories,
  Sequelize,
} = require("../models");
const gameroundhistories = require("../models/gameroundhistories");

const checkSelectedGame = (playerOne, playerTwo) => {
  let resultWin = "";
  if (playerOne === "paper") {
    if (playerTwo === "paper") resultWin = "DRAW";
    else if (playerTwo === "rock") resultWin = "paper";
    else if (playerTwo === "scissor") resultWin = "scissor";
  } else if (playerOne === "rock") {
    if (playerTwo === "paper") resultWin = "paper";
    else if (playerTwo === "rock") resultWin = "DRAW";
    else if (playerTwo === "scissor") resultWin = "rock";
  } else if (playerOne === "scissor") {
    if (playerTwo === "paper") resultWin = "scissor";
    else if (playerTwo === "rock") resultWin = "rock";
    else if (playerTwo === "scissor") resultWin = "DRAW";
  }

  return resultWin;
};

const finishGame = (e) => {
  if (e.length === 0) return null;
  let dataRound = e[0];
  let alldataRound = 0;
  let data2;
  let allData2 = 0;

  array.forEach((item) => {
    if (item === dataRound) {
      alldataRound++;
    }
  });

  array.forEach((item) => {
    if (item !== dataRound) data2 = item;

    if (item === data2) {
      allData2++;
    }
  });

  if (alldataRound > allData2) {
    return dataRound;
  } else if (alldataRound === allData2) {
    return null;
  } else {
    return data2;
  }
};

app.post(
  "/join",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const code = req.body.code;

    const room = await Rooms.findOne({
      where: {
        code: code,
      },
      include: RoomPlayers,
    });

    if (!room) {
      res.status(404).send("code invalid");
      return;
    }

    const find = room.RoomPlayers.find((room) => room.playerId === req.user.id);

    if (find) {
      res.status(500).send("user telah join");
      return;
    }

    if (room.RoomPlayers.length === 2) {
      res.status(500).send("room penuh");
      return;
    }

    const roomPlayer = await RoomPlayers.create({
      roomId: room.id,
      playerId: req.user.id,
    });
    res.send(roomPlayer);
  }
);

app.post(
  "/play",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const code = req.body.code;
    const selected = req.body.selected;

    const checkRoom = await RoomPlayers.findOne({
      where: {
        playerId: req.user.id,
      },
    });

    if (!checkRoom) {
      res.status(500).send("ini bukan room anda");
      return;
    }

    const room = await Rooms.findOne({
      where: {
        code,
      },
      include: [RoomPlayers, GameRounds],
    });

    if (!room) {
      res.status(500).send("room invalid");
      return;
    }

    console.log(room);

    if (room.RoomPlayers.length < 2 && req.user.id === room.createdBy) {
      res.status(500).send("lawan belum ada");
      return;
    }

    const gameRoundchecked = await GameRounds.findOne({
      where: {
        roomId: room.id,
        status: "ACTIVE",
      },
    });

    if (!gameRoundchecked) {
      const gameRound = await GameRounds.create({
        roomId: room.id,
        status: "ACTIVE",
      });

      const roundHistories = await GameRoundHistories.create({
        roundId: gameRound.id,
        playerId: req.user.id,
        selected: selected,
      });

      res.send(`anda memilih ${selected}`);
      return;
    }

    const checkUserRound = await GameRounds.findOne({
      where: {
        roomId: room.id,
        status: "ACTIVE",
      },
      include: GameRoundHistories,
    });

    if (checkUserRound) {
      if (checkUserRound.GameRoundHistories[0]?.playerId === req.user.id) {
        res.status(500).send("anda sudah memilih di ronde ini");
        return;
      }
    }

    const historyUser = await GameRoundHistories.create({
      roundId: room.GameRounds[0].id,
      playerId: req.user.id,
      selected: selected,
    });

    const checkSelected = await GameRoundHistories.findAll({
      where: {
        roundId: historyUser.roundId,
      },
    });

    const playerWinner = checkSelectedGame(
      checkSelected[0].selected,
      checkSelected[1].selected
    );

    let playerWinnerId;
    checkSelected.forEach((e) => {
      if (playerWinner === e.selected) {
        playerWinnerId = e.playerId;
      }
    });

    if (!playerWinnerId) {
      await GameRounds.update(
        {
          winnerId: "draw",
          status: "FINISHED",
        },
        {
          where: {
            roomId: room.id,
            status: "ACTIVE",
          },
        }
      );
    }

    if (playerWinnerId) {
      await GameRounds.update(
        {
          winnerId: playerWinnerId,
          status: "FINISHED",
        },
        {
          where: {
            roomId: room.id,
            status: "ACTIVE",
          },
        }
      );
    }

    const allRoundGame = await GameRounds.findAll({
      where: {
        roomId: room.id,
      },
    });

    if (allRoundGame.length == room.roundNum) {
      let hasil = [];
      allRoundGame.forEach((e) => {
        if (e.winnerId !== null) {
          hasil.push(e.winnerId);
        }
      });

      const pemenangnya = finishGame(hasil);
      if (pemenangnya !== null) {
        Rooms.update(
          {
            winnerId: pemenangnya,
            status: "FINISHED",
          },
          {
            where: {
              code,
            },
          }
        );
        res.send("permainan telah berakhir ");
        return;
      } else {
        Rooms.update(
          {
            winnerId: "DRAW",
            status: "FINISHED",
          },
          {
            where: {
              code,
            },
          }
        );
        res.send("permainan telah berakhir ");
        return;
      }
    }

    res.send(`pemenangnya adalah player dengan id ${playerWinnerId}`);
  }
);

app.get("/winners/:roomCode", async (req, res) => {
  const code = req.params.roomCode;

  const room = await Rooms.findOne({
    where: {
      code,
    },
  });

  res.json({
    code,
    winnerId: room.winnerId,
  });
});

module.exports = app;
