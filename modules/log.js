const express = require("express");
const app = express.Router();

const passport = require("../lib/passport");

app.get(
    "/",
    passport.authenticate("jwt", { session: false }),
    async (req, res) => {
      res.send(`Hello ${req.user.name}!`);
    }
  );

  module.exports = app