'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Rooms extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Rooms.hasMany(models.RoomPlayers, {
        foreignKey: "roomId",
      });
      models.Rooms.hasMany(models.GameRounds, {
        foreignKey: "roomId",
      });
    }
  }
  Rooms.init({
    code: {
      type: DataTypes.STRING,
      allowNull: false
    },
    roundNum: {
      type: DataTypes.STRING,
      allowNull: false
    },
    createdBy: {
      type: DataTypes.INTEGER,
      allowNull: false,
      
    },
    winnerId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false
    },
    deletedAt: {
      type: DataTypes.DATE,
      defaultValue: null
    }
  }, {
    sequelize,
    modelName: 'Rooms',
  });
  return Rooms;
};