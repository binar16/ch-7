'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RoomPlayers extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.RoomPlayers.belongsTo(models.Rooms, {
        foreignKey: "roomId",
      });
    }
  }
  RoomPlayers.init({
    roomId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      
    },
    playerId: {
      type: DataTypes.INTEGER,
      allowNull: false,
     
    },
    deletedAt: {
      type: DataTypes.DATE,
      defaultValue: null
    }
  }, {
    sequelize,
    modelName: 'RoomPlayers',
  });
  return RoomPlayers;
};